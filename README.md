Table of Contents
================
-   [About this repository](#about-this-repository)
    -   [Overview](#overview)
-   [How it works](#how-it-works)
    -   [Mass production](#mass-production)
    -   [Dealing with passes](#dealing-with-passess)
    -   [Dealing with appaeals availability](#dealing-with-appaeals-availability)
    -   [Debug](#debug)
    -   [Single report](#single-report)
    -   [Assumptions](#assumptions)
    -   [Vizualization](#vizualization)
# About this repository
This repository contains the necessary files to deploy ratio studies of the residential assessment by the CCAO. A ratio study explores the ratios between appraised values over the price of the property. You can either run a single report or run reports for all combinations of geographies, passes, and residential property types. 

# Overview 
To produce the reports, this repository somewhat follows the steps of an ETL procedure.

1. The user asks for what they want using app.R. The repo loads packages, functions and filters. It uses the renv package to standardize package usage.
2. It fetches res_diag. Short for residential diagnostic. A data frame with ratio data. 
3. It loads MTBL_RES_DIAGNOSTIC. This is a table stored in the database. It is produced in the <a href="https://gitlab.com/ccao-data-science---modeling/processes/etl_township_diagnostic"> etl_assessment_diagnostic</a> repository.
The MTBL_RES_DIAGNOSTIC table contains summary statistics for every geography, year, reporting class and pass of residential properties.
4. The repo produces and stores reports using the rmd/diagostic_report.Rmd file

# How it works

## Mass production

Let's first delve into the most likely use of this application: mass production. Let's assume you are the user. You must run the app located at app.R. If you are using R studio you must click the button run app in the toolbar. If you are using R source you must run the line 161. When you run the application a menu will pop. Select from the dropdown menu "Mass production baby". Select the year you are interested in. Submit. 

The app will create geography names and will call the function fetch_assessment over the year you have selected. fetch_assessment is a single function that fetches data for mass production and single reports. In the case of mass production, fetch_assessment will fetch large objects from the server. It will manipulate these large objects until it produces 'assessment_mass'. Then, it transforms 'assessment_mass' into a disk.frame object. A disk.frame object is a data frame stored on disk that can be manipulated from R. fetch_assessment fetches each year of 'assessment_mass' iteratively, transforms it, and saves it again on disk. Then, it binds all yearly disk.frame objects on disk. Without disk.frame, a computer with 32gb of RAM cannot handle all this data (not even using data.table). The final disk.frame object is stored in rmd folder. 

The app then sources rmd/mass_instructions.R. mass_instructions.R will transform the geography names and your input to produce all the possible combinations of parameters that can produce a report. Names at the CCAO are complicated. Some parameters have a dash ('-') and some have spaces to separate words. If you want to add another geography make sure that mass_instructions.R produces the appropriate objects. mass_instructions.R will call rmd/diagostic_report.Rmd in a loop passing each set of parameters as arguments.

rmd/diagostic_report.Rmd is a Rmarkdown file that can render reports. It requieres 5 parameters: geography, sub_geography, reporting class, year, and purpuse. It also requieres header.html and footer.html, both located at the rmd folder. header.html renders an animation that leads to the repository. footer.html renders a couple of lines with authorship and email address at the bottom of the output report. diagostic_report.Rmd will load functions and objects. If mass production is set as purpose, it will load the disk.frame object partially.

The report production loop in mass_instructions.R has 3 possible outcomes. 1) The loop can render the a report for the given set of parameters. 2) The loop can render a message indicating that there is not enough data to run a sales ratio study. The conditions and message for outcome 2 are located in rmd/objects.R. 3) The loop can render rmd/error_report.rmd after encountering an error. The error report will indicate the parameters in which the error occurred and the error message. 

## Dealing with passes

diagostic_report.Rmd can deal with the complicated pass availability. It is hard to know what passes will be available at any point. When diagostic_report.Rmd loads objects, it checks what passes are available and creates an object called available_pass. Every function used in the report can handle the available_pass object. Some functions will render part of the potential output, like pct_ratios_within_pct_fun. For the rest of the functions, I wrote loops that create text strings in markdown. These text strings are R chunks that markdown can render. These chunks are rendered as tabsets in the output report. The user can click the tabset to see the results of a specific pass. Only the available passes will produce a tabset. This efficiently solves pass availability. But, when an error occurs, markdown will indicate that the error happened in the first line, not in the line in which it actually happened. Also, you will not be able to render results in the Rmarkdown dropdown. 

## Dealing with appaeals availability
The SQL server should have appeals data for every year prior the current year. The assessor stage should produce and upload data in a regular basis. But this is not always the case. Sometimes data is not availaible when it should. The board of review is specially inconsistent with their data production. rmd/objects.R creates an object containing the most recent year with appeals data. All functions that work with appeals data will use this object as a reference.

## Debug

If there is an error related to a function that uses tabsets, error_report.rmd will render a '\n' at the end of the error message. The error happened while the function was looping through available passes. For a developer, this is not much information. Here is what you can do. Set parameters of diagostic_report.Rmd to the ones indicated in error report. But instead of setting 'Purpose' to 'Mass' set to 'Single'. Unless you have the disk.frame object in the rmd folder, you will not be able to produce 'Mass' reports. Then try the markdown line that calls the function. But change available_pass = '{{i}}' to available_pass = 'mailed'. Or another pass like 'assessor'. Then run the function. For example, you can run: 'plot_IAAO(plot_IAAO_data, params, available_pass = 'mailed')'. It is a tedious way to debug. But I don't know any other that can match this architecture. 

There is a bug in the leaflet map that I couldn't solve. If you press on another pass tab, the legend will get all over the place. If you change the first statistic in the first map, it gets solved. No idea why. 

## Single report

Let's assume you want to produce a report on a single set of parameters. Run app.R. Input the parameters you desire. The app will try to render rmd/diagnostic_report.Rmd using the parameters you have selected. If the sample is less than 30 observations, the process will stop. An rmd with a message regarding sample issues will pop. If an error occurs, an error report will be produced. This pipeline is almost similar to the mass production pipeline. The major difference is that the single report pipeline stores all objects in RAM to render the diagnostic_report.Rmd. 

## Assumptions

This repository makes a few assumptions. We assume that Board data is produced for the year prior to the current when intial values are selected. That is, if there are 2020 initial values we assume there are 2019 Board values. Functions as plot_IAAO, are affected by this assumption. 

We assume that you are running mass production on a 32 gb RAM computer. If you don't have such capability, the functions at scripts/data_ingest can be easely changed to support computers with less RAM. 

We assume that MTBL_RES_DIAGNOSTIC is updated. 

## Vizualization
Here is an image that summarises the process. The colors represent the process each pipeline follows. 
![report data scheme](rmd/images/diagnostic report scheme.png)
